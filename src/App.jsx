import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';
import DeveloperWindow from "./components/DeveloperWindow";
import AdminWindow from "./components/AdminWindow";
import AdminPoker from "./components/AdminPoker";
import DeveloperSession from "./components/DeveloperSession";
import TaskList from "./components/TaskList";

class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={DeveloperWindow} />
        <Route exact path="/session/:sessionId/:userId" component={DeveloperSession} />
        <Route exact path="/admin" component={AdminWindow} />
        <Route exact path="/admin/:sessionId/task-list" component={TaskList} />
        <Route exact path="/admin/:sessionId/task-list/:taskId" component={AdminPoker} />
      </Router>
    );
  }
}

export default App;
