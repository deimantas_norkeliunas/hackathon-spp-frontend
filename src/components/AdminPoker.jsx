import React, {Component} from 'react';
import Loader from "./loader/Loader";
import StatusLabel from "./StatusLabel";
import CopyToClipboard from "./CopyToClipboard";
import Card from "./card/Card";

class AdminPoker extends Component {
    state = {
        loading: true,
        task: null,
        results: [],
        finalRate: null,
        filteredResults: [],
        value: 0
    };
    timer;

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.fetchCurrentPokerTask();
        this.timer = setInterval(() => this.fetchCurrentPokerTask(), 10000);
    }

    componentWillUnmount() {
        this.timer = null;
    }

    handleChange(event) {
        const value = +event.target.value;
        this.setState({value});

        const res = Object.assign([], this.state.results);

        switch (value) {
            case 1:
                this.setState({filteredResults: res.filter(r => !!r.points)});
                break;
            case 2:
                this.setState({filteredResults: res.filter(r => !r.points)});
                break;
            case 0:
            default:
                this.setState({filteredResults: res});
                break;
        }
    }

    onFinishPoker = () => {
        this.setState({
            loading: true
        });
        const body = JSON.stringify({
            SessionId: +this.props.match.params.sessionId
        });
        fetch(`https://832fd102.ngrok.io/api/session/endVoting`,
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body
            })
            .then(response => {
                if (!response.ok) {
                    throw Error('Error happened on poker start');
                }
                return response;
            })
            .then(() => {
                this.props.history.push(`/admin/${this.props.match.params.sessionId}/task-list`)

            }).catch((error) => {
            this.setState({
                error: error.message,
                loading: false
            });
            console.error(error);
        });
    };

    fetchCurrentPokerTask() {
        fetch(`https://832fd102.ngrok.io/api/story/current/${this.props.match.params.sessionId}`)
            .then(response => {
                if (!response.ok) {
                    throw Error('Error happened while fetching tasks');
                }
                return response.json()
            })
            .then(result => {
                const results = result.developers.reduce((acc, dev) => {
                    const found = result.votes.find(vote => vote.userId === dev.id);
                    acc.push({...dev, points: found ? found.vote : null});
                    return acc;
                }, []);
                this.setState({
                    loading: false,
                    task: {
                        title: result.title,
                        description: result.description,
                        isClosed: result.isClosed,
                        isStarted: result.isStarted,
                        isPending: result.isPending,
                        jiraId: result.jiraId,
                        id: result.id
                    },
                    results,
                    filteredResults: Object.assign([], results),
                    finalRate: result.finalRate
                })
            }).catch((error) => {
            this.setState({
                error: error.message,
                loading: false
            })
        });
    }

    getVoteResults() {
        const listItems = this.state.filteredResults.map(vote =>
                <div className='d-flex align-items-center flex-column' key={vote.id}>
                    <Card
                        // open={cardValue === this.state.chosenCardValue}
                        open={true}
                        className='col-md-2 col-6'
                        value={vote.points}
                        onCardClick={() => {
                        }}
                        extraClass="p-2"
                    />
                    <h5>{vote.name}</h5>
                </div>
        );

        return (
            <div className='container'>
                <div className='row'>
                    {listItems}
                </div>
            </div>
        )
    }

    render() {
        if (this.state.loading) {
            return <Loader></Loader>
        } else if (!this.state.results) {
            return <div>Results not found</div>
        } else {
            return (
                <div className={'container py-4'}>
                    <div className="row">
                        <div className="col-md-4 col-12 mb-4">
                            <div className="card">
                                <div className="card-body">
                                    <a href={`https://mediapark.atlassian.net/browse/${this.state.task.jiraId}`}
                                       target={'_blank'}
                                       className='badge badge-success'
                                    >
                                        {this.state.task.jiraId || this.state.task.id}
                                    </a>
                                    <h5 className="card-title">{this.state.task.title}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted d-flex">
                                        <b>Status:</b>
                                        <div className='ml-2'>
                                            <StatusLabel task={this.state.task}/>
                                        </div>
                                    </h6>
                                    <p className="card-text">
                                        {this.state.task.description}
                                    </p>
                                </div>
                            </div>

                            <div className="card my-4">
                                <div className="card-body">
                                    Share session id
                                    <CopyToClipboard sessionId={this.props.match.params.sessionId}/>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-body">
                                    <span>
                                        Suggested task story points: {!this.state.finalRate && this.state.finalRate !== 0 ? '-' : this.state.finalRate}
                                     </span>
                                    <hr/>
                                    <button onClick={this.onFinishPoker}
                                            className="btn btn-primary mb-2">
                                        Finish poker
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8 col-12">
                            <h4 className={'d-flex align-items-center justify-content-between'}>
                                <span>
                                Vote results
                                </span>
                                <span>
                                    ({this.state.results.filter(vote => !!vote.points).length}/{this.state.results.length})
                                </span>
                            </h4>

                            <hr/>
                            <div className="form-group">
                                <select className="form-control" value={this.state.value} onChange={this.handleChange}>
                                    <option value={0}>Show all</option>
                                    <option value={1}>Finished</option>
                                    <option value={2}>Pending</option>
                                </select>
                            </div>
                            {this.getVoteResults()}

                            <hr/>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default AdminPoker;
