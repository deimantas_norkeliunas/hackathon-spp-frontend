import React, {Component} from 'react';
import Loader from "./loader/Loader.jsx";

const POST_URL = 'https://832fd102.ngrok.io/api/session/connect';

class DeveloperWindow extends Component {
    timeout = null;
    state = {
        loading: false,
        error: null,
        sessionID: '',
        username: '',
        usernameError: '',
        sessionIDError: '',
    };

    enterSession() {
        this.setState({
            loading: true,
        });

        fetch(POST_URL, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                SessionId: Number(this.state.sessionID),
                Name: this.state.username
            })
        }).then((response) => {
            if (!response.ok) {
                throw Error('Error happened while connecting to session');
            }

            return response.json();
        }).then((response) => {
            if (response.hasError) {
                throw new Error(response.errorMessage);
            } else {
                this.props.history.push(`/session/${response.sessionId}/${response.id}`);
            }
        }).catch((e) => {
            this.setState({
                error: e.message,
                loading: false,
            })
        });

    }

    handleSubmitRequest = () => {
        let usernameError = '';
        let sessionIDError = '';

        if (this.state.sessionID.length === 0) {
            sessionIDError = 'Session must not be empty';
        }

        if (this.state.username.length === 0) {
            usernameError = 'Username must not be empty';
        }

        if (usernameError || sessionIDError) {
            clearTimeout(this.timeout);

            this.setState({
                sessionIDError,
                usernameError,
            });

            this.timeout = setTimeout(() => {
                this.setState({
                    sessionIDError: '',
                    usernameError: '',
                });
            }, 5000);
        } else {
            this.enterSession();
        }
    };

    clearError = () => {
        this.setState({
            error: null,
        });
    };

    render() {
        let developerWindowUI;

        if (this.state.loading) {
            // developerWindowUI = <div>Loading session...</div>;
            return <Loader />;
        } else if (this.state.error) {
            developerWindowUI = (
                <div>
                    <div className="text-danger">Error: {this.state.error}</div>
                    <button className="btn btn-block btn-primary" onClick={this.clearError}>Ok</button>
                </div>
            )
        } else {
            developerWindowUI = (
                <div className="d-flex justify-content-center align-items-center flex-column col-12 col-md-4">
                    <div className="form-group w-100">
                        <input
                            id="sessionID"
                            type="text"
                            className="form-control my-1"
                            placeholder="Session id"
                            value={this.state.sessionID}
                            onChange={(e) => this.setState({sessionID: e.target.value})}
                            onSubmit={this.handleSubmitRequest}
                        />
                        <small className="form-text text-danger text-center">
                            {this.state.sessionIDError}
                        </small>
                    </div>
                    <div className="form-group w-100">
                        <input
                            id="username"
                            type="text"
                            className="form-control my-1"
                            placeholder="Username"
                            value={this.state.username}
                            onChange={(e) => this.setState({username: e.target.value})}
                            onSubmit={this.handleSubmitRequest}
                        />
                        <small className="form-text text-danger text-center">
                            {this.state.usernameError}
                        </small>
                    </div>
                    <div className="form-group w-100">
                        <button
                            className="btn btn-block btn-primary w-100"
                            onClick={this.handleSubmitRequest}
                        >
                            Connect
                        </button>
                    </div>
                </div>
            )
        }


        return (
            <div className="d-flex justify-content-center align-items-center flex-column vh-100 vw-100">
                {developerWindowUI}
            </div>
        );
    }
}

export default DeveloperWindow;
