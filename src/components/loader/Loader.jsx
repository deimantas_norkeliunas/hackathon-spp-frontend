import React, {Component} from 'react';
import Spinner from 'react-loader-spinner';

class Loader extends Component {
    render() {
        return (
            <div className='d-flex justify-content-center align-items-center flex-column vh-100 vw-100'>
                <Spinner type="Circles" color="#somecolor" height={80} width={80}/>
            </div>
        )
    }
}

export default Loader;
