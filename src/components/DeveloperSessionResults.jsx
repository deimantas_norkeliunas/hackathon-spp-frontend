import React, {Component} from 'react';

class DeveloperSessionResults extends Component {
    render() {
        if (this.props.finalRate === null) {
            return <div className="card p-3">No final rate is available</div>
        }

        let taskUI;

        if (this.props.task) {
            taskUI = (
                <div className="card mb-5 p-3">
                    <div>JIRA ID - {this.props.task.jiraId}</div>
                    <div>TITLE - {this.props.task.title}</div>
                    <div>DESCRIPTION - {this.props.task.description}</div>
                </div>
            );
        }

        const results = this.props.results;

        const resultsUI = results.map((result) => {
            return (
                <tr key={result.userId}>
                    <td>{result.name}</td>
                    <td>{result.vote}</td>
                </tr>
            );
        });

        return (
            <div>
                {taskUI}
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Username</th>
                            <th scope="col">Rating</th>
                        </tr>
                    </thead>
                    <tbody>
                        {resultsUI}
                        <tr>
                            <td colSpan={2}>
                                <span className="font-weight-bold">
                                    Final rate : {this.props.finalRate}
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DeveloperSessionResults;
