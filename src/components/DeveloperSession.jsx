import React, {Component} from 'react';
import DeveloperSessionResults from './DeveloperSessionResults';
import DeveloperSessionPoker from './DeveloperSessionPoker';
import Loader from "./loader/Loader";

const GET_URL = 'https://832fd102.ngrok.io/api/story/current';

const STATUS_EVALUATING = 'evaluating';
const STATUS_FINISHED = 'finished';
const STATUS_PENDING = 'pending';

const INIT_STATE = {
    loading: true,
    error: null,
    task: null,
    status: null,
    results: [],
    finalRate: null,
};


class DeveloperSession extends Component {
    constructor(props) {
        super(props);
        this.state = INIT_STATE;
        this.userId = props.match.params.userId;
        this.sessionId = props.match.params.sessionId;
    }

    getStatus(data) {
        if (data.isClosed) {
            return STATUS_FINISHED;
        }

        if (data.votes) {
            for (const devVote of data.votes) {
                if (Number(this.userId) === Number(devVote.userId)) {
                    return STATUS_PENDING;
                }
            }
        }

        return STATUS_EVALUATING;
    };

    fetchSessionData = () => {
        this.setState({
            ...INIT_STATE
        });

        fetch(`${GET_URL}/${this.sessionId}`).then(response => {
            if (!response.ok) {
                throw new Error('Error happened while fetching cards');
            }

            return response.json();
        }).then((data) => {
            console.log(data);
            let userFound = false;
            if (data.developers) {
                for (const dev of data.developers) {
                    if (Number(dev.id) === Number(this.userId)) {
                        userFound = true;
                        break;
                    }
                }
            }

            if (!userFound) {
                throw new Error('No such user in the session');
            }

            if (!data.id) {
                throw new Error('No user story available');
            }

            this.setState({
                loading: false,
                error: null,
                task: {
                    jiraId: data.jiraId,
                    id: data.id,
                    title: data.title || '',
                    description: data.description || '',
                },
                status: this.getStatus(data),
                results: data.votes || [],
                finalRate: data.finalRate,
            });
        }).catch((error) => {
            this.setState({
                error: error.message,
                loading: false,
                task: null,
                status: null,
                results: [],
                finalRate: null,
            });
        });
    };

    componentDidMount() {
        this.fetchSessionData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.state.status !== STATUS_PENDING && nextState.status === STATUS_PENDING) {
            clearInterval(this.interval);
            this.interval = setInterval(() => {
                this.fetchSessionData();
            }, 10000);
        }

        if (this.state.status === STATUS_PENDING && nextState.status !== STATUS_PENDING) {
            clearInterval(this.interval);
        }

        return true;
    }

    render() {
        let sessionUI;

        if (this.state.loading) {
            return <Loader />;
        } else if (this.state.error) {
            sessionUI = (
                <div className="card p-3">
                    <p className="text-danger">{this.state.error}</p>
                    <div>
                        <button className="btn btn-primary" onClick={this.fetchSessionData}>Try again</button>
                    </div>
                </div>
            );
        } else {
            if (this.state.status === STATUS_EVALUATING) {
                sessionUI = <DeveloperSessionPoker task={this.state.task} userId={this.userId} onContinue={this.fetchSessionData}/>;
            } else if (this.state.status === STATUS_FINISHED) {
                sessionUI = (
                    <DeveloperSessionResults
                        results={this.state.results}
                        finalRate={this.state.finalRate}
                        task={this.state.task}
                    />
                );
            } else {
                sessionUI = <div className="card p-3 text-success">Waiting for others to finish voting</div>
            }
        }

        return (
            <div className="container">
                <button className="btn btn-secondary my-2" onClick={this.fetchSessionData}>
                    Refresh session
                </button>
                {sessionUI}
            </div>
        );
    }
}

export default DeveloperSession;
