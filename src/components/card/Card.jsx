import React, {Component} from 'react';
import './Card.css';
import logo from "../../assets/images/mediapark-logo.svg";

class Card extends Component {
    handleClick = () => {
        // const currentState = this.state.open;
        // this.setState({open: !currentState});
        this.props.onCardClick();
    };

    render() {
        return (
            <div className={this.props.extraClass +  ' flip-card ' + (!this.props.open ? 'flipped' : '')} onClick={this.handleClick}>
                <div className="flip-card-inner">
                    <div className="flip-card-front">
                        {this.props.value || this.props.value === 0 ?
                            <span className={'value'}>
                            {this.props.value}
                        </span>
                            : <img src={logo} alt="mediapark-logo"/>
                        }
                    </div>
                    <div className="flip-card-back">
                        <img src={logo} alt="mediapark-logo"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;
