import React, {Component} from 'react';

class CopyToClipboard extends Component {
    state = {
        copied: false
    };
    onCopyToClipboard = () => {
        document.getElementById("session-id").select();
        document.execCommand("copy");
        this.setState({
            copied: true
        });
        setTimeout(() => {
            this.setState({
                copied: false
            });
        }, 1000)
    };

    render() {
        return (
            <div className='d-flex align-items-center'>
                {this.state.copied ?
                    <span className='text-success mr-2'>Copied</span> : null}
                <div className="input-group">
                    <input type="text" className="form-control"
                           readOnly
                           value={this.props.sessionId}
                           id='session-id'/>
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button"
                                onClick={this.onCopyToClipboard}>
                            Copy
                        </button>
                    </div>
                </div>
            </div>

        )
    }
}

export default CopyToClipboard;

