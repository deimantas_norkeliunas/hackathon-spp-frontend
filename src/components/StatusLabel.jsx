import React, {Component} from 'react';

class StatusLabel extends Component {
    getStatus() {
        const task = this.props.task;
        let status = '-';
        if (task.isStarted) {
            status = <span className='badge badge-success'>Started</span>
        } else if (task.isClosed) {
            status = <span className='badge badge-danger'>Closed</span>
        } else if (task.isPending) {
            status = <span className='badge badge-warning'>Pending</span>
        }

        return status;
    }

    render() {
        return this.getStatus();
    }
}

export default StatusLabel;
