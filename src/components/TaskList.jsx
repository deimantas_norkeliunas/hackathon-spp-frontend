import React, {Component} from 'react';
import Loader from "./loader/Loader";
import CopyToClipboard from "./CopyToClipboard";

class TaskList extends Component {
    state = {
        copied: false,
        loading: true,
        tasks: []
    };

    componentDidMount() {
        this.fetchTasks();
    }

    fetchTasks = () => {
        this.setState({
            loading: true
        });
        fetch(`https://832fd102.ngrok.io/api/story/list/${this.props.match.params.sessionId}`)
            .then(response => {
                if (!response.ok) {
                    throw Error('Error happened while fetching tasks');
                }
                return response.json()
            })
            .then(tasks => {
                console.log(tasks);
                this.setState({
                    tasks: tasks,
                    loading: false
                })
            }).catch((error) => {
            this.setState({
                error: error.message,
                loading: false
            })
        });
    };

    getStatus(task) {
        let status = '-';
        if (task.isStarted) {
            status = <span className='badge badge-primary'>Started</span>
        } else if (task.isClosed) {
            status = <span className='badge badge-success'>Estimated</span>
        } else if (task.isPending) {
            status = <span className='badge badge-warning'>Pending</span>
        }

        return status;
    }

    onStartVoting = (task) => {
        this.setState({
            loading: true
        });
        const sessionId = +this.props.match.params.sessionId;
        const taskId = +task.id;
        const body = JSON.stringify({
            SessionId: sessionId,
            StoryId: taskId
        });
        fetch(`https://832fd102.ngrok.io/api/session/startVoting`,
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body
            })
            .then(response => {
                if (!response.ok) {
                    throw Error('Error happened on poker start');
                }
                return response;
            })
            .then(() => {
                this.props.history.push(`/admin/${sessionId}/task-list/${taskId}`)
            }).catch((error) => {
            this.setState({
                error: error.message,
                loading: false
            });
            console.error(error);
        });
    };

    tableBody() {
        let listItems = this.state.tasks.map(task =>
            <tr key={task.id}>
                <th scope="row" className='text-nowrap'>{task.jiraId}</th>
                <td>{task.title}</td>
                <td className='d-none d-md-table-cell'>{task.description}</td>
                <td>{this.getStatus(task)}</td>
                <td>{task.finalRate}</td>
                <td>
                    <button onClick={() => this.onStartVoting(task)}
                            className={'btn ' + (!task.finalRate ? 'btn-success' : 'btn-outline-secondary')}
                            disabled={task.finalRate}>
                        {task.finalRate ? 'Estimated' : 'Estimate'}
                    </button>
                </td>
            </tr>
        );


        if (!listItems.length) {
            listItems = (
                <tr>
                    <td>No tasks found</td>
                </tr>
            )
        }
        return (
            <tbody>
            {listItems}
            </tbody>
        );

    }


    render() {
        if (this.state.loading) {
            return <Loader></Loader>
        } else if (this.state.error) {
            return <div className="text-danger">Error {this.state.error}</div>
        } else {
            return (
                <div className={'container'}>
                    <div className='my-4 d-flex align-items-center justify-content-between flex-column flex-md-row'>
                        <button className='btn btn-link' onClick={this.fetchTasks}>
                            Refresh list
                        </button>
                        <CopyToClipboard sessionId={this.props.match.params.sessionId}/>
                    </div>
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Task</th>
                            <th scope="col" className='d-none d-md-table-cell'>Description</th>
                            <th scope="col">Status</th>
                            <th scope="col">
                                <span className='d-none d-md-block text-nowrap'>Story points</span>
                                <span className='d-md-none'>SP</span>
                            </th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        {this.tableBody()}
                    </table>
                </div>
            )
        }
    }
}

export default TaskList;
