import React, {Component} from 'react';
import Card from "./card/Card";
import Loader from "./loader/Loader";

const URL = 'https://832fd102.ngrok.io/api/vote';
const CARDS = [0, 1, 2, 3, 5, 8, 13, 20, 40, 100];

class DeveloperSessionPoker extends Component {
    timeout = null;
    state = {
        loading: false,
        error: null,
        voted: false,
        voteError: '',
        chosenCardValue: null,
    };

    sendRating = () => {
        if (this.state.chosenCardValue === null) {
            clearTimeout(this.timeout);

            this.setState({
                voteError: 'You need to choose a card!'
            });

            this.timeout = setTimeout(() => {
                this.setState({
                    voteError: '',
                })
            }, 5000);

            return;
        }

        this.setState({
            loading: true,
            error: null,
        });

        fetch(URL, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                UserId: this.props.userId,
                StoryId: this.props.task.id,
                VoteValue: this.state.chosenCardValue,
            })
        }).then(response => {
            if (!response.ok) {
                throw new Error('Error happened while rating jira issue');
            }

            this.setState({
                loading: false,
                error: null,
                voted: true,
            });
        }).catch(e => {
            this.setState({
                loading: false,
                error: e.message,
            });
        })
    };

    handleSendRequest = (cardValue) => {
        return () => {
            this.setState(prevState => {
                let chosenCardValue;

                if (prevState.chosenCardValue === cardValue) {
                    chosenCardValue = null;
                } else {
                    chosenCardValue = cardValue;
                }

                return {
                    ...prevState,
                    chosenCardValue,
                }
            });
        }
    };

    handleClearError = () => {
        this.setState({
            error: ''
        });
    };

    render() {
        let pokerUI;

        if (this.state.loading) {
          return <Loader />;
        } else if (this.state.error) {
            pokerUI = (
                <div className="card p-3">
                    <p className="text-danger">Error: {this.state.error}</p>
                    <div>
                        <button className="btn btn-primary" onClick={this.handleClearError}>Try again</button>
                    </div>
                </div>
            );
        } else if (!this.state.voted) {
            let cardsUI;

            if (this.state.chosenCardValue !== null) {
                const cardValue = this.state.chosenCardValue;

                cardsUI = (
                    <Card
                        open={true}
                        key={cardValue}
                        value={cardValue}
                        onCardClick={this.handleSendRequest(cardValue)}
                        extraClass="p-2"
                    />
                )
            } else {
                cardsUI = CARDS.map((cardValue) => {
                    return (
                        <div className='col-md-2 col-6'>
                        <Card
                            // open={cardValue === this.state.chosenCardValue}
                            open={true}
                            key={cardValue}
                            value={cardValue}
                            onCardClick={this.handleSendRequest(cardValue)}
                            extraClass="p-2"
                        />
                        </div>
                    );
                });

            }

            pokerUI = (
                <div>
                    <div className="card p-3">
                        <div>
                            <p>{this.props.task.title}</p>
                            <p>{this.props.task.description}</p>
                        </div>
                        <div>
                            <button className="btn btn-primary" onClick={this.sendRating}>Rate</button>
                        </div>
                        <small className="form-text text-danger text-center mb-2">
                            {this.state.voteError}
                        </small>
                    </div>
                    <div className="row">
                        {cardsUI}
                    </div>
                </div>
            );
        } else {
            pokerUI = (
                <div className="card p-3">
                    <p className="text-success">Thank you for voting !</p>
                    <div>
                        <button className="btn btn-primary" onClick={this.props.onContinue}>Continue</button>
                    </div>
                </div>
            )
        }

        return pokerUI;
    }
}

export default DeveloperSessionPoker;
