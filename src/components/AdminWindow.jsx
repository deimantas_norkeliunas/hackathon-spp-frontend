import React, {Component} from 'react';
import Loader from "./loader/Loader";

class AdminWindow extends Component {
    state = {
        loading: false,
        error: null
    };

    componentDidMount() {
    }

    startSession = () => {
        this.setState({
            loading: true
        });
        fetch('https://832fd102.ngrok.io/api/session/start')
            .then(response => {
                if (!response.ok) {
                    throw Error('Error happened while starting session');
                }

                return response.json()
            })
            .then(session => {
                this.props.history.push(`/admin/${session.id}/task-list`)
            }).catch((error) => {
            this.setState({
                error: error.message,
                loading: false
            })
        });
    };

    render() {
        if (this.state.loading) {
            return <Loader></Loader>
        } else if (this.state.error) {
           return <div className="text-danger">Error {this.state.error}</div>
        } else {
            return (
                <div className='d-flex justify-content-center align-items-center flex-column vh-100 vw-100'>
                    <button onClick={this.startSession}
                            className={'btn btn-primary'}>
                        Start session
                    </button>
                </div>
            );
        }
    }
}

export default AdminWindow;
